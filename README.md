
# Simple DC Microgrid Simulation System
# 简易直流微电网模拟系统
# 介绍
&emsp;&emsp;本项目为基于TM4C123GH6PM、IR2110 BUCK驱动模块、INA282直流电流测量模块、INA333直流电压测量模块、LCD12864显示屏设计的简易直流微电网模拟系统。

&emsp;&emsp;该系统分为两部分，分别为BUCK电源变换端和负载检测端。两部分通过5m长的双绞线连接，模拟电网的电能传输。BUCK电源变换器能够实现DC-DC电源变换，负载检测器能设定输出电压、检测负载端的电压和电流并在LCD上显示，实测电压和设定电压通过蓝牙传输到BUCK电源变换器进行电压的闭环PID调节，以实现稳压输出。

&emsp;&emsp;本项目还基于MOS管和运算放大器制作了一个电子负载，该电子负载具有恒阻和恒流两种模式。恒阻模式下，可设定负载电阻为1.5-5Ω可调。恒流模式下，可设定负载电流0-2A可调。

&emsp;&emsp;此项目为华中科技大学电气工程本科课程——微控制器的课程综合任务，本人为华中科技大学的一名学生，当时刚刚上大三，对电力电子和微控制器的理解不深，解决方案粗糙，如有不妥，希望诸位批评指正。

# 开源许可
&emsp;&emsp; **本项目允许克隆及转载，但如果发布到网上的其他地方请注明出处，谢谢。** 
# 任务概述
## 任务目标
&emsp;&emsp;电能在直流微网的传输过程中会产生损耗，因而在用户端得到的电压必然会受到传输线路长度、用户负荷等因素影响。为了形象地了解直流微网的基本运行原理，本任务简单地模拟了电能产生、输电和监控过程，并在用户端监控电压和电流，反馈至电能产生部分实时调节输出。项目的系统框图如下图所示：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1108/231439_7befc284_9985353.png"/></div>

&emsp;&emsp;系统使用BUCK电源变换器模拟电能产生装置，是一种最为常用的直流降压变换器，在各类电源中应用广泛。

&emsp;&emsp;任务目标

&emsp;&emsp;1、设计和制作一个基于Tiva_C控制的BUCK电源变换器；

&emsp;&emsp;2、使用5m双绞电缆连接电源变换器和负载（电阻）；

&emsp;&emsp;3、设计和制作一个基于Tiva_C的负载监测器，监测负载电压和电流并通过无线方式反馈给电源变换器。

## 技术指标
&emsp;&emsp;1.电源变换部分输入：12V至24V，最大2A；

&emsp;&emsp;2.输出到负载（以监控部分为准）：3.3V至10V可调，分辨力0.1V，最大2A；

&emsp;&emsp;3.可在负载监控部分设定输出电压，设定的输出电压和实测电压以无线方式传输至电源变换部分，作为电源变换部分的控制反馈；

&emsp;&emsp;4.负载监控部分需检测和显示设定电压、实测电压和实测电流；

&emsp;&emsp;5.电流大于2A时电源变换部分停止输出，等待1s后重新上电；

&emsp;&emsp;6.电源变换部分输入在12V至24V变化时，输出电压变化小于2%；

&emsp;&emsp;7.负载变化导致输出电流从0至2A变化时，输出电压变化小于2%。

## 发挥部分

&emsp;&emsp;在完成上述任务后，我们将滑动变阻器负载替换为自制的基于Tiva_C的电子负载：

&emsp;&emsp;1、可调负载电流，在任何输入电压下，保持负载电流恒定；

&emsp;&emsp;2、可设定负载电流为0至2A可调，分辨力0.1A，误差不大于0.05A；

&emsp;&emsp;3、可设定负载等效电阻为1.5Ω至5Ω可调，分辨力0.1Ω，误差不大于5%+0.05Ω。

# 实现方案
## 系统整体框图
&emsp;&emsp;系统整体分为两部分：BUCK电源变换端、负载检测端。其中，BUCK电源变换端包括BUCK电源变换器和BUCK电路，负载检测端包括负载检测器和负载。我们还设计了电子负载以替代普通电阻负载来测试系统的输出性能。
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1108/232436_8c80f124_9985353.png"/></div>

## 代码结构
&emsp;&emsp;整个项目的代码也一样分为两部分：BUCK电源变换端、负载检测端。其中BUCK电源变换端对应Master分支下的工程文件，负载检测端对应DC_Load_Monitoring分支下的工程文件。下面分别对两部分的代码结构进行介绍。
### BUCK电源变换端
&emsp;&emsp;我们采用模块化编程的思路，把各个功能的实现算法独立封装成一个库文件，只要设计好接口头文件，我们就可以很方便地在主程序里面进行调用，其代码结构如下图所示。
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1108/233227_94e46c55_9985353.png"/></div>

### 负载检测端
&emsp;&emsp;负载检测端同样采用模块化编程的思路，把各个功能的实现算法独立封装成一个库文件，只要设计好接口头文件，我们就可以很方便地在主程序里面进行调用，代码结构如下图所示。
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/121004_5eef45f6_9985353.png"/></div>

# 硬件设计
## BUCK电源变换端
&emsp;&emsp;BUCK电源变换端包括BUCK电源变换器和BUCK电路两部分，其硬件连接示意图如下所示：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/094344_91f95f4a_9985353.png"/></div>

## 负载检测端
&emsp;&emsp;负载检测端包括负载检测器和负载两部分，其中负载部分使用自己设计的电子负载代替，其硬件连接示意图如下所示：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/094734_bef06b4c_9985353.png"/></div>

&emsp;&emsp;其中。电子负载的原理图如下图所示。
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/094829_6b2ccceb_9985353.png"/></div>

&emsp;&emsp;其原理为：

&emsp;&emsp;1、恒流模式(CC mode)。电阻R1为采样电阻，其阻值为1Ω，因此R1两端的电压即负载电流，通过电流测量单元测得负载电流之后，Tiva_c控制器通过DAC输出到运放U1的同相输入端，并与反相输入端的负载电流进行比较，Tiva_c控制器里用软件设置PI调节器实现负反馈调节，使得负载电流满足设定值。

&emsp;&emsp;2、恒压模式(CV mode)。在恒压工作模式时，电子负载所流入的负载电流依据所设定的负载电压而定，此时负载电流将会增加直到负载电压等于设定值为止，即负载电压保持设定值不变。

&emsp;&emsp;3、恒阻模式(CR mode)。通过负载电压和设定电阻值计算出所需的负载电流，然后通过负反馈调节负载电流直到负载电阻满足设定值。

# 软件设计
&emsp;&emsp;这一章主要对核心代码及其思路做一个梳理，我会指出每部分代码所对应的文件，清楚思路之后就可以自行查阅和修改。
## BUCK电源变换端
&emsp;&emsp;BUCK电源变换端的代码位于Master分支
### 主程序
&emsp;&emsp;主程序对应main.c文件，是按照时间来进行调度的，对于每一个关键功能的实现，都有严格的时间间隔规定，采用前后台查询的机制，根据事件的标志位决定是否运行相应的处理程序。

&emsp;&emsp;程序开始之后，进行初始化，包括各个系统时钟设置及各个模块时钟的初始化、浮点运算单元初始化、定时器模块初始化、PWM初始化、PID运算初始化、UART初始化等。接着进入主循环，等待标志到来，执行每20ms一次的任务，具体的工作将在后面详细说明。蓝牙接收则使用接收中断。程序流程如下：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/123504_0537849f_9985353.png"/></div>


 **20ms执行一次的任务流程：** 

&emsp;&emsp;定时器每20ms将标志位置1，在主程序里循环判断该事件是否执行。每20ms执行一次的事件包括：过电流检查、PID运算、PWM输出，同时清除标志位。程序流程如下：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/121404_6829b75d_9985353.png"/></div>

### PID调节
&emsp;&emsp;PID调节对应pid.c文件和pwm_output.c文件，通过调节PWM的占空比来调节BUCK电路的输出电压跟设定电压相一致。
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/121518_a4d69f5c_9985353.png"/></div>

### 蓝牙接收
&emsp;&emsp;蓝牙接收对应uartreceive.c文件，蓝牙通讯协议：数据位为8位，波特率为9600，无奇偶校验位。每次发送3个浮点数Uo、Io、Us，将Uo、Io、Us乘100赋值给三个长整数，每个长整数拆分成4个字符发送，接收端对应变换为浮点数。接收端流程如下图所示：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/104032_fa9b9afe_9985353.png"/></div>

## 负载检测端
&emsp;&emsp;负载检测端的代码位于DC_Load_Monitoring分支
### 主程序
&emsp;&emsp;主程序对应main.c文件，同样采用前后台查询的机制。程序开始之后，进行初始化，包括各个系统时钟设置及各个模块时钟的初始化、浮点运算单元初始化、定时器模块初始化、ADC模块初始化、LCD初始化、PWM初始化、PID运算初始化、UART初始化、电子负载DAC初始化、按键程序初始化等。接着进入主循环，等待标志到来，分别执行20ms、1s的各种任务， 具体每个时间段的工作将在后面详细说明。流程图如下：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/121640_899dc181_9985353.png"/></div>

 **20ms执行一次的任务流程：** 

&emsp;&emsp;在主程序里循环判断该事件是否执行。每20ms执行一次的事件包括：测量Uo、Io，蓝牙发送Uo、Io、Us，过电流检查。程序流程如下：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/121751_3e0fd4d3_9985353.png"/></div>


 **1s执行一次的任务流程：** 

&emsp;&emsp;在主程序里循环判断该事件是否执行。每1s执行一次LCD屏幕刷新。程序流程如下：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/121842_5bc60f94_9985353.png"/></div>


&emsp;&emsp;在主程序里循环扫描键盘是否被按下，如果键盘被按下则执行相应的动作。程序流程如下：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/122018_5fb1973a_9985353.png"/></div>

### ADC采样
&emsp;&emsp;负载检测端ADC采样对应measure.c文件，由定时器触发中断，每2ms触发一次采样序列。使用采样序列0，FIFO深度为8，前4个用于采样电压值然后取平均，后4个用于采样电流值然后取平均。
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/122159_d09aa449_9985353.png"/></div>

### 按键及其显示
 **键盘扫描流程** 

&emsp;&emsp;按键扫描对应key.c文件，我们采用矩阵键盘来执行设定电压、设定电流、设定电阻的更改，并实时显示到LCD上。键盘扫描，如果有按键被按下，则记录矩阵键盘上被按下的按键的行列值。键盘扫描流程如下：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/122334_d78869ba_9985353.png"/></div>

 **变量更改及显示流程** 

&emsp;&emsp;变量更改及其显示对应lcd.c文件，每次键盘扫描获得的输入数字显示在LCD上，其他按键值则不显示，输入时显示光标。以电压更改为例，流程如下：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/122515_6f062f2f_9985353.png"/></div>


### 蓝牙发送
&emsp;&emsp;蓝牙发送对应uartsend.c文件，蓝牙通讯协议：数据位为8位，波特率为9600，无奇偶校验位。每次发送3个浮点数Uo、Io、Us，将Uo、Io、Us乘100赋值给三个长整数，每个长整数拆分成4个字符发送，接收端对应变换为浮点数。发送端流程如下：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/122604_f50e5ee7_9985353.png"/></div>

### 显示界面
&emsp;&emsp;显示界面对应lcd.c文件，开机后，界面先显示开机提示，然后在界面左边显示实测值，右边显示设定值。定时器每隔1s把刷新标志位置1。流程如下：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/122703_162f9ab9_9985353.png"/></div>


### 电量计算
&emsp;&emsp;电量计算对应measure.c文件，电量经过ADC采样，每个采样序列电压和电流中间值取了4次平均，然后每十个采样周期更新一次电压和电流（取十次平均），所以每个电量的计算取了40次平均。然后根据Tiva测到的值跟电压表测的值进行线性拟合，提高测量的精度。流程如下：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/122754_ee828d0a_9985353.png"/></div>


### 电子负载
&emsp;&emsp;电子负载对应eletronic.c文件，主要是判断恒流还是恒阻模式，通过线性拟合提高精度，转换到对应的数字量，并使用DAC7512芯片输出对应的模拟电压来控制电子负载的硬件部分。流程如下：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/122849_2d2f7135_9985353.png"/></div>


# 测试结果分析
## 测试方案及设备
&emsp;&emsp;整机系统测试在室内进行，各种电能测量仪器齐全；硬件部分TM4C123GH6PM作为主控制器；软件使用Code Composer Studio 6.2.0作为开发环境。使用的测试仪器如下表所示：
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/120132_a08f115b_9985353.png"/></div>

## 测试结果
（1）负载约为10Ω时的输出电压变化
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/120307_af646b7f_9985353.png"/></div>

（2）设定电压为6V，通过改变负载大小检测输出电压变化
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/120316_9115ad80_9985353.png"/></div>

（3）改变输入电压，保持负载电流恒定（预设为1A）
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/120324_e9905e22_9985353.png"/></div>

（4）设定负载电流为0-2A可调并记录实测电流
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/120330_b502981e_9985353.png"/></div>

（5）设定负载等效电阻为1.5Ω-5Ω可调并记录实际电阻
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/120338_aa066e09_9985353.png"/></div>

## 结果分析
<div align=center><img src="https://images.gitee.com/uploads/images/2021/1109/123038_b446613a_9985353.png"/></div>